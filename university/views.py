from django.shortcuts import render
from .schema import schema
from .models import Student
import json
# Create your views here.
def home(request):
    result = schema.execute('''
    query test {
        student(id: 1) {
            name
        }
    }

    ''')
    result1 = dict(result.data.items())
    return render(request, 'home.htm', {'result' : json.dumps(result1, indent=4), 'test' : test})