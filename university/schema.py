import graphene
from graphene_django.types import DjangoObjectType, ObjectType
from .models import Student, Faculty

class StudentType(DjangoObjectType):
    class Meta:
        model = Student

class FacultyType(DjangoObjectType):
    class Meta:
        model = Faculty

class Query(ObjectType):
    student = graphene.Field(StudentType, id=graphene.Int())
    faculty = graphene.Field(FacultyType, id=graphene.Int())

    def resolve_student(self, info, **kwargs):
        id = kwargs.get('id')

        if id is not None:
            return Student.objects.get(pk=id)

        return None

    def resolve_faculty(self, info, **kwargs):
        id = kwargs.get('id')

        if id is not None:
            return Faculty.objects.get(pk=id)

class FacultyInput(graphene.InputObjectType):
    id = graphene.ID()
    name = graphene.String()

class StudentInput(graphene.InputObjectType):
    id = graphene.ID()
    name = graphene.String()
    faculty = graphene.Field(FacultyInput)

class CreateStudent(graphene.Mutation):
    class Arguments:
        input = StudentInput(required=True)

    ok = graphene.Boolean()
    student = graphene.Field(StudentType)

    @staticmethod
    def mutate(root, info, input=None):
        ok = True
        faculty = Faculty.objects.get(pk=input.faculty.id)
        student_instance = Student(name=input.name, faculty=faculty)
        student_instance.save()
        return CreateStudent(ok=ok, student=student_instance)

class UpdateStudent(graphene.Mutation):
    class Arguments:
        id = graphene.Int(required=True)
        input = StudentInput(required=True)

    ok = graphene.Boolean()
    student = graphene.Field(StudentType)

    @staticmethod
    def mutate(root, info, id, input=None):
        ok = False
        student_instance = Student.objects.get(pk=id)
        if student_instance:
            ok = True
            student_instance.name = input.name
            student_instance.faculty = Faculty.objects.get(pk=input.faculty.id)
            student_instance.save()
            return UpdateStudent(ok=ok, student=student_instance)
        return UpdateStudent(ok=ok, student=None)

class CreateFaculty(graphene.Mutation):
    class Arguments:
        input = FacultyInput(required=True)

    ok = graphene.Boolean()
    faculty = graphene.Field(FacultyType)

    @staticmethod
    def mutate(root, info, input=None):
        ok = True
        faculty_instance = Faculty(name=input.name)
        faculty_instance.save()
        return CreateFaculty(ok=ok, faculty=faculty_instance)

class UpdateFaculty(graphene.Mutation):
    class Arguments:
        id = graphene.Int(required=True)
        input = FacultyInput(required=True)

    ok = graphene.Boolean()
    faculty = graphene.Field(FacultyType)

    @staticmethod
    def mutate(root, info, id, input=None):
        ok = False
        faculty_instance = Faculty.objects.get(pk=id)
        if faculty_instance:
            ok = True
            faculty_instance.name = input.name
            faculty_instance.save()
            return UpdateFaculty(ok=ok, faculty=faculty_instance)
        return UpdateFaculty(ok=ok, faculty=None)

class Mutation(graphene.ObjectType):
    create_student = CreateStudent.Field()
    update_student = UpdateStudent.Field()
    create_faculty = CreateFaculty.Field()
    update_faculty = UpdateFaculty.Field()


schema = graphene.Schema(query=Query, mutation=Mutation)





    